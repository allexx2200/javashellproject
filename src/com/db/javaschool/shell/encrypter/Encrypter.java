package com.db.javaschool.shell.encrypter;

public interface Encrypter {
    byte[] encrypt(byte[] message, byte[] key);
    byte[] decrypt(byte[] encriptedMessage, byte[] key);
}
