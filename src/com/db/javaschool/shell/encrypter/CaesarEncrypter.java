package com.db.javaschool.shell.encrypter;

public class CaesarEncrypter implements Encrypter{
    @Override
    public byte[] encrypt(byte[] message, byte[] key) {
        byte[] result = new byte[message.length];
        for(int i = 0; i < result.length; i++){
            result[i] = (byte) (message[i] + key[i % key.length]);
        }
        
        return result;
    }

    @Override
    public byte[] decrypt(byte[] encriptedMessage, byte[] key) {
        byte[] result = new byte[encriptedMessage.length];
        for(int i = 0; i < result.length; i++){
            result[i] = (byte) (encriptedMessage[i] - key[i % key.length]);
        }
        
        return result;
    }
    
}
