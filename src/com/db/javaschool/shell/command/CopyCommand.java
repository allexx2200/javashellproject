package com.db.javaschool.shell.command;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import com.db.javaschool.shell.encrypter.Encrypter;

public class CopyCommand extends AbstractCommand {
    private Encrypter enc = null;
    private byte[] key = null;
    public enum ENC{
        ENCRYPT, DECRYPT;
    }
    private ENC enm;
    
    public CopyCommand(String parameters, Encrypter enc, byte[] key, ENC enm) {
        super(parameters.trim());
        this.enc = enc;
        this.enm = enm;
    }

    
    public CopyCommand(String parameters) {
        this(parameters, null, null, null);
    }

    @Override
    public void execute() {
        String[] args = getParameters().split(" ");
        if(args == null || args.length < 2){
            System.out.println("Invalid parameter count. Usage: cp [destination] [source]");
            return;
        }
        
        String source = args[1];
        String destination = args[0];
        
        
        InputStream is = null;
        OutputStream os = null;
        
        int sourcePathIndex = -1;
        
        String currentPath = System.getProperty("user.dir");
        for(int i = 0; i < source.length(); i++){
            if(source.charAt(i) == '\\' || source.charAt(i) == '/'){
                sourcePathIndex = i;
            }
        }
        
        int destinationPathIndex = -1;
        for(int i = 0; i < destination.length(); i++){
            if(destination.charAt(i) == '\\' || destination.charAt(i) == '/'){
                destinationPathIndex = i;
            }
        }
        
        String sourcePath = null;
        String sourceFileName = null;
        if(sourcePathIndex != -1){
            sourcePath = source.substring(0, sourcePathIndex);
            sourceFileName = source.substring(sourcePathIndex + 1);
        } else {
            sourceFileName = source;
        }
        
        String destinationPath = null;
        String destinationFileName = null;
        if(destinationPathIndex != -1){
            destinationPath = destination.substring(0, destinationPathIndex);
            destinationFileName = destination.substring(sourcePathIndex + 1);
        } else {
            destinationFileName = destination;
        }
        
        try {
            
            if(sourcePath != null){
                new ChangeDirectoryCommand(sourcePath).execute();
            }
            is = new FileInputStream(sourceFileName);
            new ChangeDirectoryCommand(currentPath).execute();
            if(destinationPath != null){
                new ChangeDirectoryCommand(destinationPath).execute();
            }
            os = new FileOutputStream(destinationFileName);
            new ChangeDirectoryCommand(currentPath).execute();
            
            byte[] buffer = new byte[1024];
            if(enc != null){
                if(enm == ENC.ENCRYPT){
                    buffer = enc.encrypt(buffer, key);
                } else {
                    buffer = enc.decrypt(buffer, key);
                }
            }
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find specified file.");
        } catch (IOException e) {
            System.out.println("Error ocured during writting.");
        } finally {
            try {
                is.close();
                os.close();
            } catch (IOException | NullPointerException e) {}
        }
    }

}
