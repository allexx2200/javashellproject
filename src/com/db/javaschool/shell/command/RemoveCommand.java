package com.db.javaschool.shell.command;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

public class RemoveCommand extends AbstractCommand{

    public RemoveCommand(String parameters) {
        super(parameters.trim());
    }

    @Override
    public void execute() {
        String[] filePaths = getParameters().split(" ");
        if(filePaths == null || filePaths.length < 1){
            System.out.println("Invalid number of parameters. Usage: rm [file...]");
            return;
        }
        
        boolean recursive = false;
        for(String path : filePaths){
            if(path.compareTo("-r") == 0){
                recursive = true;
            }
        }
        
        for(String path : filePaths){
            if(path.compareTo("-r") == 0){
                continue;
            }
            
            try {
                File f = new File(path);
                Files.delete(Paths.get(f.getCanonicalPath()));
            } catch (NoSuchFileException e) {
                System.out.format("%s: no such" + " file or directory%n", path);
            } catch (DirectoryNotEmptyException e) {
                if(recursive){
                    System.out.println("Recursive delete not implmented.");
                    continue;
                } else {
                    System.out.format("Directory %s not empty%n", path);
                }
            } catch (IOException x) {
                System.out.println("Can't remove file. Do not have enough permisions.");
            }
        }
    }

}
