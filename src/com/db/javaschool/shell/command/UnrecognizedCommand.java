package com.db.javaschool.shell.command;
/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: Unrecognized command statement
 */
public class UnrecognizedCommand extends AbstractCommand {
    private static final String UNRECOGNIZED_COMMAND_MESSAGE = "Unrecognized command";
    
    public UnrecognizedCommand(String parameters) {
        super(parameters);
    }
    
    public UnrecognizedCommand() {
        super(null);
    }

    @Override
    public void execute() {
       System.out.println(UNRECOGNIZED_COMMAND_MESSAGE);
    }

}
