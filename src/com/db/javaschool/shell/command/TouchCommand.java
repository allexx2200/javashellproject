package com.db.javaschool.shell.command;

import java.io.File;
import java.io.IOException;

public class TouchCommand extends AbstractCommand {
    private static final String INVALID_PARAM_COUNT = "Invalid number of paramters. Usage: touch [file path...].";
    private static final String CANT_CREATE_FILE = "Can't create the desired file. Cause: ";
    
    public TouchCommand(String parameters) {
        super(parameters.trim());
    }

    @Override
    public void execute() {
        if(getParameters().compareTo("") == 0){
            System.out.println(INVALID_PARAM_COUNT);
            return;
        }
        String currentPath = System.getProperty("user.dir");
        String[] files = getParameters().split(" ");
        for(String filePath : files){
            int index = -1;
            for(int i = 0; i < filePath.length(); i++){
                if(filePath.charAt(i) == '\\' || filePath.charAt(i) == '/'){
                    index = i;
                }
            }
            
            String path = null;
            String fileName = null;
            if(index != -1){
                path = filePath.substring(0, index);
                fileName = filePath.substring(index + 1);
            } else {
                fileName = filePath;
            }
            
            if(path != null){
                new ChangeDirectoryCommand(path).execute();
            }
            createFile(System.getProperty("user.dir") + File.separator + fileName);
            if(path != null){
                new ChangeDirectoryCommand(currentPath).execute();
            }
        }
    }
    
    private void createFile(String path){
        System.out.println("Path: " + path);
        File f = new File(path);
        try{
            f.getParentFile().mkdirs();
        } catch(NullPointerException e){}
        
        try {
            f.createNewFile();
        } catch (IOException e) {
            System.out.println(CANT_CREATE_FILE + e.getMessage());
        }
    }
    

}
