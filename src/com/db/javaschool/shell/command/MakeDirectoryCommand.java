package com.db.javaschool.shell.command;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MakeDirectoryCommand extends AbstractCommand {
    private static final String INVALID_PARAM_COUNT = "Invalid number of paramters. Usage: mkdir [dir path...].";
    public MakeDirectoryCommand(String parameters) {
        super(parameters.trim());
    }

    @Override
    public void execute() {
        if(getParameters().compareTo("") == 0){
            System.out.println(INVALID_PARAM_COUNT);
            return;
        }
        
        String[] dirs = getParameters().split(" ");
        String currentPath = System.getProperty("user.dir");
        
        for(String directoryPath : dirs){
            String[] pathDirs = directoryPath.split("\\\\");
            for(String s : pathDirs){
                createDirectory(s);
                new ChangeDirectoryCommand(s).execute();
            }
            
            new ChangeDirectoryCommand(currentPath).execute();
        }
    }

    private void createDirectory(String dirPath){
        try {
            File f = new File(dirPath);
            if(f.isAbsolute()){
                Files.createDirectories(Paths.get(dirPath));
            } else {
                Files.createDirectories(Paths.get(f.getAbsolutePath()));
            }
        } catch (IOException e) {}
    }
    
}
