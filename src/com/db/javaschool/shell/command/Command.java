package com.db.javaschool.shell.command;
/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: Command interface for defining the functionality of a command.
 */
public interface Command {
    /**
     * Execute the command
     */
    void execute();
}
