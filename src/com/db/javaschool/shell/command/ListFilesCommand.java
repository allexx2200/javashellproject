package com.db.javaschool.shell.command;

import java.io.File;


public class ListFilesCommand extends AbstractCommand {
    private static final char LIST_CHAR = 'l';
    private static final char ALL_CHAR = 'a';
    private static final char HR_CHAR = 'h';
    private static final char RECURSIVE_CHAR = 'r';

    public ListFilesCommand(String parameters) {
        super(parameters.trim());
    }

    @Override
    public void execute() {
        String[] args = getParameters().split(" ");
        boolean list = false;
        boolean all = false;
        boolean humanReadable = false;
        boolean recursive = false;
        String path = null;
        for (String s : args) {
            if (s.length() > 0 && s.charAt(0) == '-') {
                for (int i = 1; i < s.length(); i++) {
                    if (s.charAt(i) == LIST_CHAR) {
                        list = true;
                    } else if (s.charAt(i) == ALL_CHAR) {
                        all = true;
                    } else if (s.charAt(i) == HR_CHAR) {
                        humanReadable = true;
                    } else if (s.charAt(i) == RECURSIVE_CHAR){
                        recursive = true;
                    } else {
                        System.out.println("Unknown parameter " + s.charAt(i) + "\nUsage: ls [-lahr] [path]");
                        return;
                    }
                }
            } else {
                if (path == null) {
                    path = s;
                } else {
                    System.out.println("Too many parameters." + "\nUsage: ls [-lah] [path]");
                    return;
                }
            }
        }

        String current = System.getProperty("user.dir");

        if (path == null || path.compareTo("") == 0) {
            path = System.getProperty("user.dir");
        } else {
            new ChangeDirectoryCommand(path).execute();
            path = System.getProperty("user.dir");
        }
        File folder = new File(path);
        System.out.println("Path: " + path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (!list) {
                if (listOfFiles[i].isFile()) {
                    System.out.print("[F]" + listOfFiles[i].getName() + " ");
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.print("[D]" + listOfFiles[i].getName() + " ");
                    if(recursive){
                        System.out.println("Recursive show not implemented.");
                        continue;
                    }
                }
            } else {
                if (!all && listOfFiles[i].isHidden()){
                    continue;
                }
                String size = null;
                if(listOfFiles[i].isDirectory()){
                    size = "          ";
                } else {
                    long sz = listOfFiles[i].length();
                    if(!humanReadable){
                        size = "" + sz;
                        while(size.length() < 10){
                            size = " " + size;
                        }
                    } else {
                        if(sz / (1 << 30) > 0){
                            size = ((double)((int)(((double)sz / (1 << 30))*100)))/100 + "Gb";
                        } else if(sz / (1 << 20) > 0){
                            size = ((double)((int)(((double)sz / (1 << 20))*100)))/100 + "Mb";
                        } else if(sz / (1 << 10) > 0){
                            size = ((double)((int)(((double)sz / (1 << 10))*100)))/100 + "Kb";
                        } else {
                            size = sz + "b";
                        }
                        while(size.length() < 10){
                            size = " " + size;
                        }
                    }
                }
                
                if (listOfFiles[i].isFile()) {
                    System.out.print("[F] ");
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.print("[D] ");
                } else {
                    System.out.print("[?] ");
                }
                
                if(listOfFiles[i].canRead()){
                    System.out.print("r");
                } else {
                    System.out.print("-");
                }
                
                if(listOfFiles[i].canWrite()){
                    System.out.print("w");
                } else {
                    System.out.print("-");
                }
                
                if(listOfFiles[i].canExecute()){
                    System.out.print("x");
                } else {
                    System.out.print("-");
                }
                
                System.out.println("  " + size + "  " + listOfFiles[i].getName());
            }
        }
        System.out.println();

        new ChangeDirectoryCommand(current).execute();
    }

}
