package com.db.javaschool.shell.command;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ChangeDirectoryCommand extends AbstractCommand {
    private static final String DIR_NOT_FOUND = "Can't change to desired directory. Directory does not exist";
    
    public ChangeDirectoryCommand(String parameters) {
        super(parameters.trim());
    }

    @Override
    public void execute() {
        String destination = getParameters();
        File current = new File(System.getProperty("user.dir"));
        
        File aux = new File(destination);
        
        File dst = null;
        if(aux.isAbsolute()){
            dst = aux;
        } else {
            dst = new File(current, destination);
        }
        String absoluteDestination = null;
        try {
            absoluteDestination = dst.getCanonicalPath();
        } catch (IOException e) {
            System.out.println(DIR_NOT_FOUND);
            return;
        }
        Path path = Paths.get(absoluteDestination);
        
        if (Files.exists(path)) {
            System.setProperty("user.dir", absoluteDestination);
        } else {
            System.out.println(DIR_NOT_FOUND);
        }
    }

}
