package com.db.javaschool.shell.command;
/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: Abstract command parser class for root implementation of the command parser
 */
public abstract class AbstractCommandParser {
    /**
     * @param buffer the introduced command
     * @return a Command object from the parsed buffer
     */
    public abstract Command parseCommand(String buffer);
}
