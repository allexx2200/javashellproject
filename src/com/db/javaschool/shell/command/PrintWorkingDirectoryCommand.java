package com.db.javaschool.shell.command;
/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: Abstract command parser class for root implementation of the command parser
 */
public class PrintWorkingDirectoryCommand extends AbstractCommand {
    public PrintWorkingDirectoryCommand(String parameters) {
        super(parameters);
    }
    
    public PrintWorkingDirectoryCommand(){
        this(null);
    }

    @Override
    public void execute() {
        System.out.println(System.getProperty("user.dir"));
    }

}
