package com.db.javaschool.shell.command;

public class ClearCommand extends AbstractCommand {
    public static final String CLEAR_SEQUENCE = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    
    public ClearCommand(String parameters) {
        super(parameters);
    }
    
    public ClearCommand() {
        this(null);
    }

    @Override
    public void execute() {
        System.out.println(CLEAR_SEQUENCE);
    }

}
