package com.db.javaschool.shell.command;

import com.db.javaschool.shell.encrypter.CaesarEncrypter;

/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: The actual command parser used
 */
public class CommandParser extends AbstractCommandParser {
    private static final String EXIT_SEQUENCE = "exit";
    private static final String PWD_SEQUENCE = "pwd";
    private static final String CLEAR_SEQUENCE = "clear";
    private static final String TOUCH_SEQUENCE = "touch";
    private static final String MKDIR_SEQUENCE = "mkdir";
    private static final String CD_SEQUENCE = "cd";
    private static final String CAT_SEQUENCE = "cat";
    private static final String LS_SEQUENCE = "ls";
    private static final String CP_SEQUENCE = "cp";
    private static final String RM_SEQUENCE = "rm";
    private static final String CAESAR_ENCRPT = "encryption caesar";
    
    @Override
    public Command parseCommand(String buffer) {
        if(buffer == null || buffer.compareTo("") == 0){
            return null;
        }
        
        /* check for exit command */
        if(isCommand(buffer, EXIT_SEQUENCE)){
            return new ExitCommand();
        } else if(isCommand(buffer,PWD_SEQUENCE)){
            return new PrintWorkingDirectoryCommand();
        } else if(isCommand(buffer, CLEAR_SEQUENCE)){
            return new ClearCommand();
        } else if(isCommand(buffer, TOUCH_SEQUENCE)){
            return new TouchCommand(buffer.substring(TOUCH_SEQUENCE.length()));
        } else if(isCommand(buffer, MKDIR_SEQUENCE)){
            return new MakeDirectoryCommand(buffer.substring(MKDIR_SEQUENCE.length()));
        } else if(isCommand(buffer, CD_SEQUENCE)){
            return new ChangeDirectoryCommand(buffer.substring(CD_SEQUENCE.length()));
        } else if(isCommand(buffer, CAT_SEQUENCE)){
            return new CatCommand(buffer.substring(CAT_SEQUENCE.length()));
        } else if(isCommand(buffer, LS_SEQUENCE)){
            return new ListFilesCommand(buffer.substring(LS_SEQUENCE.length()));
        } else if(isCommand(buffer, CP_SEQUENCE)){
            return new CopyCommand(buffer.substring(CP_SEQUENCE.length()));
        } else if(isCommand(buffer, RM_SEQUENCE)){
            return new RemoveCommand(buffer.substring(RM_SEQUENCE.length()));
        } else if(isCommand(buffer, CAESAR_ENCRPT)){
           // byte[] b = {1};
            //return new CopyCommand(buffer.substring(CAESAR_ENCRPT.length()), new CaesarEncrypter(), b, CopyCommand.ENC.ENCRYPT);
            
        }
        
        return new UnrecognizedCommand();
    }
    
    private boolean isCommand(String buffer, String command){
        return buffer.length() >= command.length() && buffer.substring(0, command.length()).compareTo(command) == 0;
    }
}
