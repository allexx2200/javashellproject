package com.db.javaschool.shell.command;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CatCommand extends AbstractCommand {
    private static final String CANT_OPEN_FILE = "File cannot be opened. It either does not exist or you do not have enough permissions to acces it.";
    public CatCommand(String parameters) {
        super(parameters.trim());
    }

    @Override
    public void execute() {
        String currentPath = System.getProperty("user.dir");
        
        int index = -1;
        String s = getParameters();
        String fileName = null;
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '\\'){
                index = i;
            }
        }
        
        if(index != -1){
            String tempPath = s.substring(0, index - 1);
            new ChangeDirectoryCommand(tempPath).execute();
            fileName = s.substring(index + 1, s.length());
        } else {
            fileName = s;
        }
        
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();

            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
            br.close();
        }catch(IOException e){
            System.out.println(CANT_OPEN_FILE);
            System.out.println("File: " + getParameters());
        }
        
        if(index != -1){
            new ChangeDirectoryCommand(currentPath).execute();
        }
        
    }

}
