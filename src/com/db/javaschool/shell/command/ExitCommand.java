package com.db.javaschool.shell.command;

import com.db.javaschool.shell.entry.Shell;
/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: The exit command for terminating the shell
 */
public class ExitCommand extends AbstractCommand{
    private static final String CLOSING_SHELL_MESSAGE = "Closing shell!";
    
    public ExitCommand(String parameters) {
        super(parameters);
    }

    public ExitCommand() {
        this(null);
    }

    
    @Override
    public void execute() {
        Shell.closeTerminal();
        System.out.println(CLOSING_SHELL_MESSAGE);
    }

}
