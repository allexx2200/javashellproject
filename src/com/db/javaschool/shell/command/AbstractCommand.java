package com.db.javaschool.shell.command;
/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: Abstract command for implementing common functionality 
 */
public abstract class AbstractCommand implements Command {
    private String parameters;
    
    /**
     * The parameters of the given command
     * @param parameters
     */
    public AbstractCommand(String parameters){
        this.parameters = parameters;
    }

    /**
     * @return the paraters given to the command
     */
    public String getParameters() {
        return parameters;
    }
}
