package com.db.javaschool.shell.entry;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;
import com.db.javaschool.shell.command.AbstractCommandParser;
import com.db.javaschool.shell.command.Command;
import com.db.javaschool.shell.command.CommandParser;

/**
 * @author Alexandru-Marian Atanasiu, email: alexandru.atanasiu@outlook.com
 * @since 09 March 2016
 * @version 1.0
 * 
 * DESCRIPTION: Entry point of the shell project.
 */
public class Shell {  
    public static final String CLEAR_SEQ = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    private static boolean activated = true; 
    
    public static void main(String[] args) {
        showStartup();
        
        /* finding the user and the local machine */
        String user = null; 
        String machine = null;
        String path = null;
        try {
            user = System.getProperty("user.name");
            path = System.getProperty("user.dir");
            machine = java.net.InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            machine = "workstation";
        }
        
        /* openning scanner stream */
        Scanner s = new Scanner(System.in);
        
        /* Creation of the command factory */
        AbstractCommandParser commandFactory = new CommandParser();
        
        /* main loop */
        while(activated){
            System.out.print(user + "@" + machine + ":[" + path + "] ~ $ ");
            String buffer = s.nextLine();
            
            Command command = commandFactory.parseCommand(buffer.trim());
            if(command != null){
                command.execute();
            }
            
            path = System.getProperty("user.dir");
        }
        
        /* close scanner stream */
        s.close();
    }
    
    public static void showStartup(){
        /* clear the screen*/
        System.out.println(CLEAR_SEQ);
        
        try {
            BufferedReader in = new BufferedReader(new FileReader("computer_ascii.txt"));         
            for(String line = in.readLine(); line != null; line = in.readLine()){
                System.out.println(line);
               
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            System.out.println("\n");
            in.close();
        } catch (IOException e) { e.printStackTrace(); System.out.println("ERROR!");}
    }
    
    /**
     * Close the terminal function
     */
    public static void closeTerminal(){
        activated = false;
    }
}